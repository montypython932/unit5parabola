import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Parabola extends JFrame {
	
	public Parabola() {
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new GraphPanel());
		setTitle("Parabola");
		setSize(400, 400);
	}
	
	public static void main(String[] args) {
		Parabola par = new Parabola();
	}
	
	public class GraphPanel extends JPanel {
		
		public GraphPanel() {
			
		}
		
		@Override
		public void paintComponent(Graphics g) {
			Polygon p = new Polygon();
			super.paintComponent(g);
			double scaleFactor = 0.1;
			for (int x=-100; x<=100; x++) {
				p.addPoint(x+200, 200- (int)(scaleFactor *x *x));
			}
			g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
			g.drawLine(0, 200, 400, 200);
			g.drawLine(200, 0, 200, 400);
			g.drawString("X axis", 10, 215);
			g.drawString("Y axis", 205, 10);
			g.drawString("Origin", 205, 220);
		}
	}
}
